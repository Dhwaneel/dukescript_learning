package com.DukeLearning;

import net.java.html.boot.BrowserBuilder;

public final class Main {

    private Main() {
    }

    //Shows index.html in a browser window
    public static void main(String... args) throws Exception {
        //context can be sent here as parameter newBrowser(Object... context)
        BrowserBuilder.newBrowser().
                loadPage("pages/index.html").
                loadClass(Main.class).
                //browser gets ready and invokes onPageLoad
                invoke("onPageLoad", args).
                showAndWait();
        System.exit(0);
    }

    /**
     * Called when the page is ready.
     */
    public static void onPageLoad() throws Exception {
        DataModel.onPageLoad();
    }

}
