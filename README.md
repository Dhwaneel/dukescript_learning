# README #
Prerequisites: JDK 1.8+ and Maven.

Building and Running the app:

1. Go to the root of project (that contains parent pom.xml) and run from cmd line:`mvn clean install`

2. To run app in Desktop, go the 'client' folder and run from cmd line:
`mvn process-classes exec:exec`

3. To run app in a Browser, go the 'client-web' folder and run from cmd line:
`mvn package bck2brwsr:show`

4. To run app in iPhone simulator (assuming you have xcode installed), go the 'client-ios' folder and run from cmd line (similarly to run in iPad use ipad-sim in place of iphone-sim):
`mvn package robovm:iphone-sim`